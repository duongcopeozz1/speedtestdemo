package com.volio.baitap3.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.volio.baitap3.R;
import com.volio.baitap3.TableSymblo;
import com.volio.baitap3.adapter.RecycleSymbloAdapter;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SymbloFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SymbloFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SymbloFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SymbloFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SymbloFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SymbloFragment newInstance(String param1, String param2) {
        SymbloFragment fragment = new SymbloFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    ArrayList<TableSymblo> listSymblo;
    public static int current;
    Context context;
    private Button btn ;
    private ImageView iconView;
    private ImageView iconView2;
    private TextView title;
    private TableSymblo tableSymblo;
    SharedPreferences sharedPreferences;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        context =getContext();
        sharedPreferences=context.getSharedPreferences("view_mode",Context.MODE_PRIVATE);
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_symblo, container, false);
        RecyclerView recyclerView=v.findViewById(R.id.recycle_view);
        listSymblo=TableSymblo.createArray();
        RecycleSymbloAdapter recycleSymbloAdapter=new RecycleSymbloAdapter(listSymblo);
        recyclerView.setAdapter(recycleSymbloAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

//        RecycleSymbloAdapter adapter = new RecycleSymbloAdapter(listSymblo, )

        recycleSymbloAdapter.setDetailAdapterListener(new RecycleSymbloAdapter.DetailAdapterListener() {
            @Override
            public boolean onClickBtn(int position,View view) {
                 tableSymblo=listSymblo.get(position);
                DecimalFormat decimalFormat=new DecimalFormat("#.##");
                boolean check;
                 btn        =view.findViewById(R.id.btn_use);
                 iconView =view.findViewById(R.id.ic_check_mask);
                 iconView2   =view.findViewById(R.id.ic_check_mask2);
                 title      =view.findViewById(R.id.txt_view1);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                if(tableSymblo.isBtn_used()==false){
                    check=true;
                    btn.setText(R.string.btn_used);
                    btn.setBackground(context.getResources().getDrawable(R.drawable.btn_used));
                    tableSymblo.setBtn_used(true);
                    title.setText(tableSymblo.getValue1() + "%(" + decimalFormat.format(tableSymblo.getValue2()) + "x) Mặc định");
                    iconView.setVisibility(view.VISIBLE);
                    iconView2.setVisibility(view.VISIBLE);
                    //luu
                    editor.putString("view_mode1",btn.getText().toString().trim());
                    editor.putString("view_mode2",btn.getBackground().toString());
                    editor.putBoolean("view_mode3",tableSymblo.isBtn_used());
                    editor.putString("view_mode4",title.getText().toString().trim());
                    editor.putInt("view_mode5",iconView.getVisibility());
                    editor.putInt("view_mode6",iconView2.getVisibility());


                }
                else
                {
                    check=false;
                    btn.setText(R.string.btn_not_used);
                    title.setText(tableSymblo.getValue1() + "%(" + decimalFormat.format(tableSymblo.getValue2()) + "x)");
                    iconView.setVisibility(view.GONE);
                    iconView2.setVisibility(view.GONE);
                    tableSymblo.setBtn_used(false);
                    btn.setBackground(context.getResources().getDrawable(R.drawable.btn_not_used));
                    editor.putString("view_mode1",btn.getText().toString().trim());
                    editor.putString("view_mode2",btn.getBackground().toString().trim());
                    editor.putBoolean("view_mode3",tableSymblo.isBtn_used());
                    editor.putString("view_mode4",title.getText().toString().trim());
                    editor.putInt("view_mode5",iconView.getVisibility());
                    editor.putInt("view_mode6",iconView2.getVisibility());
                }
                editor.commit();
                return check;
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
//        btn.setText(sharedPreferences.getString("view_mode1",""));
//        title.setText(sharedPreferences.getString("view_mode4",""));
//        iconView.setVisibility(sharedPreferences.getInt("view_mode5",0));
//        iconView2.setVisibility(sharedPreferences.getInt("view_mode6",0));
//        tableSymblo.setBtn_used(sharedPreferences.getBoolean("view_mode3",false));
//        String name=sharedPreferences.getString("view_mode2","");
//        int id =getResources().getIdentifier(name,"drawable","com.volio.baitap3.fragment");
//        btn.setBackground(getResources().getDrawable(id));
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
