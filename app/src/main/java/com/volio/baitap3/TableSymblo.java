package com.volio.baitap3;

import java.util.ArrayList;

public class    TableSymblo {
    private float value1;
    private float value2;
    private float size;
    private boolean btn_used;

    public TableSymblo(float value1, float value2, float size, boolean btn_used) {
        this.value1 = value1;
        this.value2 = value2;
        this.size = size;
        this.btn_used = btn_used;
    }

    public float getValue1() {
        return value1;
    }

    public void setValue1(float value1) {
        this.value1 = value1;
    }

    public float getValue2() {
        return value2;
    }

    public void setValue2(float value2) {
        this.value2 = value2;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public boolean isBtn_used() {
        return btn_used;
    }

    public void setBtn_used(boolean btn_used) {
        this.btn_used = btn_used;
    }

    public static ArrayList<TableSymblo> createArray() {
        float arrnumber[]=new float[]{0.9f,1.0f,1.1f,1.3f,1.5f,1.7f,2.0f,2.2f};
        ArrayList<TableSymblo> listSymblo = new ArrayList<>();
        float    size=15;
        for (int i = 0; i < 8; i++) {
            listSymblo.add(new TableSymblo(arrnumber[i]*100, arrnumber[i], size*arrnumber[i], false));

        }
        return listSymblo;
    }

}
