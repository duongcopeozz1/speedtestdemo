package com.volio.baitap3.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.volio.baitap3.Contact;
import com.volio.baitap3.R;

import java.util.List;

public class ContactStatusAdapter extends RecyclerView.Adapter<ContactStatusAdapter.ViewHolder> {
    @NonNull
    @Override
    public ContactStatusAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView=(CardView)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_contact,viewGroup,false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        CardView cardView=viewHolder.cardView;
        TextView txtContact=cardView.findViewById(R.id.txt_contact);
        final Button btnContact=cardView.findViewById(R.id.btn_status);
        txtContact.setText(mContacts.get(i).getName());
        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btnContact.getText().equals("Online")) btnContact.setText("Offline");
                else btnContact.setText("Online");
            }
        });
    }

    @Override
    public int getItemCount() {
       return mContacts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(@NonNull CardView itemView) {
            super(itemView);
            cardView=itemView;
        }
    }
    private List<Contact> mContacts;
    public ContactStatusAdapter(List<Contact> contacts) {
        mContacts=contacts;
    }

}
