package com.volio.baitap3.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.volio.baitap3.R;
import com.volio.baitap3.TableSymblo;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;


public class RecycleSymbloAdapter extends RecyclerView.Adapter<RecycleSymbloAdapter.ViewHolder> {
    private List<TableSymblo> listSymblo;
    TextView txt_v1;
    TextView txt_v2;
    TextView txt_v3;
    TextView txt_v4;
    public TableSymblo tableSymblo;
    public Button btn;
    public Context context;
    float size;
    public DetailAdapterListener detailAdapterListener;


    public void setDetailAdapterListener(DetailAdapterListener detailAdapterListener) {
        this.detailAdapterListener = detailAdapterListener;
    }

    @NonNull
    @Override
    public RecycleSymbloAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.item_view, parent, false);
        return new ViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        tableSymblo = listSymblo.get(i);
        btn = viewHolder.btn;
        final LinearLayout linearLayout = viewHolder.linearLayout;
        txt_v1 = linearLayout.findViewById(R.id.txt_view1);
        txt_v2 = linearLayout.findViewById(R.id.txt_view2);
        txt_v3 = linearLayout.findViewById(R.id.txt_view3);
        txt_v4 = linearLayout.findViewById(R.id.txt_view4);
        size = tableSymblo.getSize();
        final DecimalFormat decimalFormat = new DecimalFormat("#.#");
        BigDecimal roundfinalPrice = new BigDecimal((size)).setScale(2, BigDecimal.ROUND_HALF_UP);
        Float newSize = roundfinalPrice.floatValue();
        txt_v1.setText(tableSymblo.getValue1() + "%(" + decimalFormat.format(tableSymblo.getValue2()) + "x)");
        txt_v4.setTextSize(newSize);
        txt_v3.setTextSize(newSize + 5.0f);
        txt_v2.setTextSize(newSize + 10.0f);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //minh biry la da bam vao vi tri i
//                if(tableSymblo.isBtn_used()==false){
//                    btn.setText(R.string.btn_used);
//                    btn.setBackground(context.getResources().getDrawable(R.drawable.btn_used));
//                    tableSymblo.setBtn_used(true);
//                }
//                else
//                {
//                    btn.setText(R.string.btn_not_used);
//                    tableSymblo.setBtn_used(false);
//                    btn.setBackground(context.getResources().getDrawable(R.drawable.btn_not_used));
//                }
                if (detailAdapterListener != null) {
                    boolean value = detailAdapterListener.onClickBtn(i, linearLayout);


                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listSymblo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout linearLayout;
        private Button btn;

        public ViewHolder(@NonNull LinearLayout itemView) {
            super(itemView);
            linearLayout = itemView;
            btn = itemView.findViewById(R.id.btn_use);

        }
    }

    public RecycleSymbloAdapter(List<TableSymblo> listSymblo) {
        this.listSymblo = listSymblo;

    }

    public interface DetailAdapterListener {
        boolean onClickBtn(int position, View v);
    }
}
